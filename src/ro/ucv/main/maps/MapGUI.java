package ro.ucv.main.maps;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.jdesktop.swingx.JXMapKit;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.Waypoint;
import org.jdesktop.swingx.mapviewer.WaypointPainter;
import org.jdesktop.swingx.mapviewer.WaypointRenderer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;
import java.util.TreeSet;

public class MapGUI extends JFrame implements MouseListener {

	private JTextField longitude = new JTextField();
	private JTextField latitude = new JTextField();
	private JTextField radius = new JTextField();
	private JTextField dots = new JTextField();

	private double ovalRadius = 0;
	private Double lastZoom;
	private Point lastClick = null;
	private GeoPosition lastClickedLocation = new GeoPosition(0, 0);
	private WaypointPainter waypointPainter = new WaypointPainter();
	private JXMapKit map;
	
	private Random rand;
	private final int noOfRandomDots = 25;

	public MapGUI() 
	{
		super("Open Map - needs internet connection");

		this.setVisible(true);
		this.setBounds(150, 150, 850, 650);
		this.setLayout(null);

		map = (JXMapKit) (new SetupMap ()).createOpenMap();
		map.setBounds(20, 20, 600, 300);
		map.getMainMap().addMouseListener(this);
		add(map);

		JLabel lblLong = new JLabel("Longitude");
		JLabel lblLat = new JLabel("Latitude");
		JLabel lblRadius = new JLabel("Radius");
		JLabel lblDots = new JLabel("Dots inside circle");
		lblLong.setBounds(20, 330, 150, 25);
		lblLat.setBounds(180, 330, 150, 25);
		lblRadius.setBounds(340, 330, 150, 25);
		lblDots.setBounds(500, 330, 150, 25);
		add(lblLong);
		add(lblLat);
		add(lblRadius);
		add(lblDots);

		longitude.setBounds(20, 365, 150, 25);
		latitude.setBounds(180, 365, 150, 25);
		radius.setBounds(340, 365, 150, 25);
		dots.setBounds(500, 365, 150, 25);
		add(longitude);
		add(latitude);
		add(radius);
		add(dots);
			
		repaint();
		
		addRandomPoints();

		// Add a sample waypoint.

		waypointPainter.setRenderer(new WaypointRenderer() 
		{
			@Override
			public boolean paintWaypoint(Graphics2D g, JXMapViewer map,
					Waypoint wp) {
				float alpha = 0.6f;
				int type = AlphaComposite.SRC_OVER;
				AlphaComposite composite = AlphaComposite.getInstance(type,
						alpha);
				Color color = new Color(1, 0, 0, alpha); // Red

				g.setColor(color);
				g.fillOval(-5, -5, 10,10);
				g.setColor(Color.BLACK);
				g.drawOval(-5, -5, 10,10);
				
				if (wp.getPosition().getLatitude() == lastClickedLocation.getLatitude() && wp.getPosition().getLongitude() == lastClickedLocation.getLongitude())
				{
					paintCircle(g, map, color);
				}
				
				return true;
			}

		});
	}

	@SuppressWarnings("unchecked")
	@Override
	public void mouseClicked(MouseEvent e) {

		JXMapViewer m = map.getMainMap();
		GeoPosition g = m.convertPointToGeoPosition(e.getPoint());

		System.out.println(" geo coordinates:  " + g);
		latitude.setText("" + g.getLatitude());
		longitude.setText("" + g.getLongitude());

		if (e.getButton() == MouseEvent.BUTTON1) {

			lastClick = e.getPoint();
			lastClickedLocation = g;
			ovalRadius = 0;
			moveWaypoint(g);

		} else {
			if (lastClick != null) {
				radius.setText("" + geoDistance(lastClickedLocation, g));
				ovalRadius = ((Double) (lastClick.distance(e.getPoint()))).doubleValue();
				lastZoom=lastClick.distance(map.getMainMap().getCenter());
				dots.setText(countPoints(m) + "/" + noOfRandomDots);
				
			} else
				moveWaypoint(g);

		}
		repaint();
	}
	
	public int countPoints(JXMapViewer m)
	{
		int counter = -1;
		Iterator it = waypointPainter.getWaypoints().iterator();
		while (it.hasNext())
		{
			GeoPosition g = ((Waypoint)it.next()).getPosition();
			Point2D p = m.convertGeoPositionToPoint(g);
			
			if (Math.pow(p.getX() - lastClick.getX(), 2) + Math.pow(p.getY() - lastClick.getY(), 2) < ovalRadius * ovalRadius)
			{
				counter++;
			}
		}
		
		return counter;

	}
	
	public void paintCircle(Graphics2D g, JXMapViewer map, Color color)
	{
		if (ovalRadius > 0) 
		{			
			Double currentZoom=lastClick.distance(map.getCenter());
			Double r=currentZoom/lastZoom *ovalRadius;
			int zoomedRadius=r.intValue();
			System.out.println("radius "+ovalRadius+"   zoomed"+zoomedRadius);
			g.setColor(color);
			g.fillOval(-zoomedRadius, -zoomedRadius, 2 * zoomedRadius,
					2 * zoomedRadius);
			g.setColor(Color.RED);
			g.drawOval(-zoomedRadius, -zoomedRadius, 2 * zoomedRadius,
					2 * zoomedRadius);
		}
	}

	/**
	 * this draws on the map at clicked location . 
	 * @param g
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void moveWaypoint(GeoPosition g) {
		
		//Modify this if you need to add a set of markers instead of a single one.
		if (waypointPainter.getWaypoints().size() <= noOfRandomDots)
		{
			waypointPainter.getWaypoints().add(new Waypoint(g.getLatitude(), g.getLongitude()));
		}
		else
		{
			ArrayList al = new ArrayList(waypointPainter.getWaypoints());
			waypointPainter.getWaypoints().remove(al.get(al.size() - 1));		
			waypointPainter.getWaypoints().add(new Waypoint(g.getLatitude(), g.getLongitude()));
		}

		map.getMainMap().setOverlayPainter(waypointPainter);

	}
	
	public void addRandomPoints()
	{
		for (int i = 0; i < noOfRandomDots; i++)
		{
			rand = new Random();
			Waypoint wp = new Waypoint(new GeoPosition(44.32 + rand.nextGaussian() / 50, 23.80 + rand.nextGaussian() / 50));
			//points.add(wp);
			waypointPainter.getWaypoints().add(wp);
		}
		map.getMainMap().setOverlayPainter(waypointPainter);
	}

	public double geoDistance(GeoPosition g1, GeoPosition g2) {
		final int EARTHRADIUS = 6371; // The radius of the earth in kilometers

		// Get the distance between latitudes and longitudes
		double deltaLat = Math.toRadians(g1.getLatitude() - g2.getLatitude());
		double deltaLong = Math
				.toRadians(g1.getLongitude() - g2.getLongitude());

		// Apply the Haversine function
		double a = Math.sin(deltaLat / 2) * Math.sin(deltaLat / 2)
				+ Math.cos(Math.toRadians(g2.getLatitude()))
				* Math.cos(Math.toRadians(g1.getLatitude()))
				* Math.sin(deltaLong / 2) * Math.sin(deltaLong / 2);
		return EARTHRADIUS * 2 * Math.asin(Math.sqrt(a));
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		// System.out.println("entered");
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		// System.out.println("exited");

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		// System.out.println("pressed");

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		// System.out.println("releaseed");

	}
}
